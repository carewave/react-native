module.exports = {
    preset: 'react-native',
    roots: ['<rootDir>'],
    setupFiles: ["./jest/setup.js"],
    transform: {
      '.+\\.(css|scss)$': 'jest-transform-stub',
      '^.+\\.jsx$': 'babel-jest',
    },
    moduleNameMapper: {
        "\\.(jpg|jpeg|png|svg)$": "src/config/fileMock.js"
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.js?$',
    moduleFileExtensions: ['js', 'jsx'],
    transformIgnorePatterns: [
        "/node_modules/(?!react-native)/.+"
    ],
    modulePaths: ['<rootDir>'],
};