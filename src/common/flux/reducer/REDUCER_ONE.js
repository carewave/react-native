import { $ACTION_TYPE$ } from '../action/types';

export default function countryReducer(state = [], action) {
    switch (action.type) {
      case $ACTION_TYPE$:
        return [...state, action.$ACTION_DATA$];
      default:
        return state;
    }
}