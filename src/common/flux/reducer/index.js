import { combineReducers } from 'redux';
import $ACTION_DATA$ from './$REDUCER_ONE$';

export default combineReducers({
    $ACTION_DATA$: $ACTION_DATA$
});