import { $ACTION_TYPE$ } from './types';

export const $ACTION_NAME$ = $ACTION_DATA$ => ({
  type: $ACTION_TYPE$,
  $ACTION_DATA$
})
