import { createStackNavigator } from 'react-navigation';
import Intro from '../../intro/index.js';

const AppNavigator = createStackNavigator({
    Intro: {screen: Intro,
      navigationOptions: {
        header: null
      }},
    initialRouteName: 'Intro'

    
});

export default AppNavigator;