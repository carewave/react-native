package org.volme;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {

    @SuppressLint("StaticFieldLeak")
    private static Context mainContext;

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "volme";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainContext = this;
    }

    public static Context getMainContext(){
        return mainContext;
    }
}
