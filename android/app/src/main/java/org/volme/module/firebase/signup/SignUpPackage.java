package org.volme.module.firebase.signup;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SignUpPackage implements ReactPackage {

    private SignUp signUp;

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();

        modules.add(initDeviceConnector(reactContext));

        return modules;
    }

    private SignUp initDeviceConnector(ReactApplicationContext reactContext){
        signUp = new SignUp(reactContext);
        return signUp;
    }

    public SignUp getDeviceConnector() {
        return signUp;
    }

}
