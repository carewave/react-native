package org.volme.module.firebase.signup;

import android.app.Activity;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import org.volme.module.common.data.database.FireBaseDBInitializer;
import org.volme.module.firebase.signup.logic.PhoneAuthorizer;

import javax.annotation.Nonnull;

import static org.volme.MainActivity.getMainContext;

public class SignUp extends ReactContextBaseJavaModule {


    public SignUp(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "SignUp";
    }

    @ReactMethod
    public void startSmsVerfication(String phoneNumber, Callback successCallback) {
        System.out.println(phoneNumber);
        System.out.println(successCallback);

        // db init
        FireBaseDBInitializer.create().init();
        PhoneAuthorizer.init((Activity) getMainContext(), phoneNumber).registerUser();
    }


}
