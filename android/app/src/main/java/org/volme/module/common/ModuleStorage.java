package org.volme.module.common;

import com.facebook.react.shell.MainReactPackage;
import org.volme.module.firebase.signup.SignUpPackage;

public class ModuleStorage {

    private static ModuleStorage instance;

    private MainReactPackage mainReactPackage;
    private SignUpPackage signUpPackage;

    private ModuleStorage(){
        mainReactPackage = new MainReactPackage();
        signUpPackage = new SignUpPackage();
    }

    public static ModuleStorage getModuleStorage(){
        if(instance == null){
            instance = new ModuleStorage();
        }
        return instance;
    }

    public MainReactPackage getMainReactPackage() {
        return mainReactPackage;
    }

    public SignUpPackage getSignUpPackage() {
        return signUpPackage;
    }

}
