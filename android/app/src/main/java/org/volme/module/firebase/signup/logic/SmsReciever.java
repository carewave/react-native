package org.volme.module.firebase.signup.logic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsReciever extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();
            SmsMessage[] smsMessages;
            String msgFrom;
            if (bundle != null){
                //---retrieve the SMS message received---
                Object[] pdus = (Object[]) bundle.get("pdus");
                smsMessages = new SmsMessage[pdus.length];
                for(int i=0; i<smsMessages.length; i++){
                    smsMessages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    msgFrom = smsMessages[i].getOriginatingAddress();
                    if (msgFrom != null && msgFrom.equalsIgnoreCase("Phone code")) {
                        String msgBody = smsMessages[i].getMessageBody();
                        PhoneAuthorizer.getInstance().getVerificationDataFromFirestoreAndVerify(msgBody.split("\\D+")[0]);
                        break;
                    }
                }
            }
        }
    }
}
