# Volme React-Native mobile client

## Description
The project allows to build client app of Volme project on mobile platforms including Android, iOs platforms.
Main idea of this initiative is to keep mobile client development in "one place". It resolves issue with unified UI, its
developing and maintaning.

### Stack of technologies
UI part:
1) IDE: Visual Studio Code
2) ReactNative
3) Node server (minimal 8 version)
4) Cypress
5) Jest
6) CSS
7) Webpack
8) Babel

Android part:
1) IDE: Android Studio
2) Java 8
3) Min android SDK 16
4) Gradle 3

iOs part:
1) IDE: xCode
2) Swift compiler
3) MacOs minimal version: Sierra

## How to get started
Below you will find a short instruction list to run react-native environment with project inside.
It's designed for cases when dev host does not have all necessar tools. If you see that some tool, lib is already installed on your machine,
then avoid this step and go next.

1) Download and install Visual Code Studio ( Link: https://code.visualstudio.com/download )
2) in current folder: git clone https://gitlab.com/carewave/react-native.git .
Stack of commands below is better to run inside of Visual Code Studio
3) npm install -g react-native-cli
4) npm install
5) npm audit fix
6) npm start
7) For each platform following set of commands will be different.
    Android platform...
        Make sure you have connected device to your dev machine or you have up and running android emulator (can be run from Android Studio)
        Finally run in Visual Code Studio:  react-native run-android
   
    iOs platform:
        Make sure you have installed xCode IDE with "instruments" package.
        Once it's checked verify if you have one connected iphone device to your macbook. If not react-native will create iOs emulator automatically.
        Finally run in Visual Code Studio: react-native run-ios

If all goes properly you will have nodejs server run on port 8081. From it react-native will transfer appropriate for certain platform app(apk) file to your device
or emulator.

## Faced issues during start
1) Sometimes npm works not like it is expected from him. From this moment you begin observing strange error messages about not found resources, packages, libs.
In such cases you can try to execute: "npm audit fix". If it does not help please remove package-lock.json file and node_modules folder. After that please run again: 
npm install

2) If you can not run node server because 8081 port is already used please run: npm stop
Stop command was implemented in package.json file in scripts section. Basically it kills all run node processes on local machine.

## Useful links
Official facebook tutorial page: https://facebook.github.io/react-native/docs/getting-started

## License
Commercial I guess